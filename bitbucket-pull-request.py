#!/usr/bin/env python

"""
Bitbucket command tool to help many common tasks involving pull requests.

Usage:

	pr [<options>] <command> [<args>]

Options:

	-h, --help
		Display help message.

	-r <repo>, --repo <repo>
		Use this repo instead of upstream or origin. If no repo option is provided, it will use 'upstream' as repo name.
		In case there is no remote upstream found in the current repo, it will use 'origin' as repo name.

	-d --debug
		Enable debug mode.

	-u <reviewer>, --reviewer <reviewer>
		Send pull request and assign it to this reviewer. This can either be the username or the UUID of the account

	-t <targetbranch>, --targetbranch <targetbranch>
		Send pull requests to this targetbranch.


Commands:

	#no command#
		Displays a list of the open pull requests on this repository.

	help
		Displays this message.

	open
		Opens bitbucket pull request page for the current repo.

	open [<pull request ID>]
		Opens either the current pull request or the specified request on
		bitbucket.

	submit [<pull body>] [<pull title>]
		Pushes a branch and sends a pull request to the user's reviewer on
		bitbucket.
"""


import base64
import getopt
import json
import os
import re
import sys
import requests
import urllib3
from uuid import UUID

from textwrap import fill

options = {
	# Color Scheme
	'color-success': 'green',
	'color-status': 'blue',
	'color-error': 'red',
	'color-warning': 'red',
	'color-display-title-url': 'cyan',
	'color-display-title-number': 'magenta',
	'color-display-title-text': 'red',
	'color-display-title-user': 'blue',
	'color-display-info-repo-title': 'default',
	'color-display-info-repo-count': 'magenta',
	'color-display-info-total-title': 'green',
	'color-display-info-total-count': 'magenta',

	'debug': False,

	# Determines whether to open a browser for pull request on bitbucket
	'submit-open-bitbucket': True

}

def bitbucket_json_request(url, params = None):
	session = requests.Session()
	session.headers.update({'Content-Type': 'application/json; charset=utf-8'})

	authorize_request(session)
	if DEBUG:
		print url

	try:
		if params is not None:
			data = json.dumps(params)
			request = session.post(url, data)
		else:
			request = session.get(url)
	except urllib3.HTTPError, msg:
		raise UserWarning("Error communicating with bitbucket: %s\n%s" % (url, msg))

	data = request.text
	if data == '':
		raise UserWarning("Invalid response from bitbucket")

	data = json.loads(data)

	if DEBUG:
		print json.dumps(data, sort_keys=True, indent=4)

	return data

def color_text(text, token, bold = False):
	"""Return the given text in ANSI colors"""

	# http://travelingfrontiers.wordpress.com/2010/08/22/how-to-add-colors-to-linux-command-line-output/

	color_name = options["color-%s" % token]

	if color_name == 'default' or not sys.stdout.isatty():
		return text

	colors = (
		'black', 'red', 'green', 'yellow',
		'blue', 'magenta', 'cyan', 'white'
	)

	if color_name in colors:
		return u"\033[{0};{1}m{2}\033[0m".format(
			int(bold),
			colors.index(color_name) + 30,
			text)
	else:
		return text


def command_help():
	print __doc__


def open_URL(url):
	os.system('open -g "%s"' % url)


def display_pull_request(pull_request):
	"""Nicely display_pull_request info about a given pull request"""

	display_pull_request_minimal(pull_request)
	print "	%s" % color_text(pull_request.get('links').get('html').get('href'), 'display-title-url')

	if pull_request.get('description').strip():
		print fill(pull_request.get('description'), initial_indent="	", subsequent_indent="	", width=80)

	# print "   Created: %s" % date.strftime(isodate.parse_datetime( pull_request.get('issue_created_at')), "%B %d, %Y at %I:%M %p")
	# print "   Created: %s" % pull_request.get('issue_created_at')
	# print isodate.parse_datetime( pull_request.get('issue_created_at'), "%Y-%m-%dT%H:%M:%S" )

	print

def build_pull_request_title():
	"""Returns the last commit message"""
	title = os.popen('git log -1 --no-color --reverse --format=%s').read()
	return title

def display_pull_request_minimal(pull_request):
	"""Display minimal info about a given pull request"""

	print "%s - %s by %s (%s)" % (color_text("REQUEST %s" % pull_request.get('id'), 'display-title-number', True), color_text(pull_request.get('title'), 'display-title-text', True), color_text(pull_request['author'].get('display_name'), 'display-title-user'), pull_request['author'].get('username'))


def authorize_request(session):
	session.headers.update({'Authorization': "Basic %s" % auth_string})


def load_options():
	all_config = os.popen('git config -l').read().strip()

	matches = re.findall("^git-pull-request\.([^=]+)=([^\n]*)$", all_config, re.MULTILINE)
	for k in matches:
		value = k[1]

		if value.lower() in ('f', 'false', 'no'):
			value = False
		elif value.lower() in ('t', 'true', 'yes'):
			value = True
		elif value.lower() in ('', 'none', 'null', 'nil'):
			value = None

		options[k[0]] = value


def get_default_repo_name():
	repo_name = get_repo_name_for_remote('upstream')

	# fallback to origin
	if repo_name is None or repo_name == '':
		repo_name = get_repo_name_for_remote('origin')

	if repo_name is None or repo_name == '':
		raise UserWarning("Failed to determine bitbucket repository name")

	return repo_name


def get_repo_name_for_remote(remote_name):
	"""Returns the repository name for the remote with the name"""

	remotes = os.popen('git remote -v').read()
	m = re.search("^%s[^\n]+?bitbucket\.org[^\n]*?[:/]([^\n]+?)\.git" % remote_name, remotes, re.MULTILINE)

	if m is not None and m.group(1) != '':
		return m.group(1)

def get_pull_requests(repo_name):
	"""Returns information retrieved from bitbucket about the open pull requests on
	the repository"""

	url = "https://api.bitbucket.org/2.0/repositories/%s/pullrequests" % repo_name
	data = bitbucket_json_request(url)

	return data['values']

def get_pull_request(repo_name, pull_request_ID):
	"""Returns information retrieved from bitbucket about the pull request"""

	url = "https://api.bitbucket.org/2.0/repositories/%s/pullrequests/%s" % (repo_name, pull_request_ID)
	data = bitbucket_json_request(url)

	return data

def get_current_branch_name():
	"""Returns the name of the current pull request branch"""
	branch_name = os.popen('git rev-parse --abbrev-ref HEAD').read().strip()
	return branch_name


def display_status():
	"""Displays the current branch name"""

	branch_name = get_current_branch_name()
	print "Current branch: %s" % branch_name


def command_show(repo_name):
	"""List open pull requests

	Queries the bitbucket API for open pull requests in the current repo.
	"""

	print color_text("Loading open pull requests for %s" % repo_name, 'status')
	print

	pull_requests = get_pull_requests(repo_name)

	if len(pull_requests) == 0:
		print "No open pull requests found"

	for pull_request in pull_requests:
		display_pull_request(pull_request)

	display_status()


def command_open(repo_name, pull_request_ID = None):
	"""Open a pull request in the browser"""

	url = 'https://bitbucket.org/%s/pull-requests' % repo_name;
	if pull_request_ID is not None:
		pull_request = get_pull_request(repo_name, pull_request_ID)
		url = pull_request['links']['html']['href']
	open_URL(url)


def validate_branch_name():
	branch_name = get_current_branch_name()

	if 'master' in branch_name or 'dev-branch' in branch_name or 'RC-' in branch_name:
		raise UserWarning("Invalid branch: can't submit pull request with branch name: %s" % branch_name)


def is_valid_uuid(uuid, version=4):
	try:
		uuid_obj = UUID(uuid, version=version)
	except:
		return False

	return str(uuid_obj) == uuid


def command_submit(repo_name, username, target_branch, reviewer = None, pull_body = None, pull_title = None, submitOpenBitbucket = True):
	"""Push the current branch and create a pull request to your bitbucket reviewer
	(or upstream)"""

	validate_branch_name()
	branch_name = get_current_branch_name()
	source_repo = get_repo_name_for_remote('origin')

	print color_text("Submitting pull request for %s" % branch_name, 'status')

	print color_text("Pushing local branch %s to origin" % branch_name, 'status')

	ret = os.system('git push origin %s' % branch_name)

	if ret != 0:
		raise UserWarning("Could not push this branch to your origin")

	url = "https://api.bitbucket.org/2.0/repositories/%s/pullrequests" % repo_name

	if pull_title == None or pull_title == '':
		pull_title = build_pull_request_title()

	if pull_body == None:
		pull_body = ''

	if target_branch == None:
		target_branch = 'dev-branch'

	params = {
		"title": pull_title,
		"source": {
			"branch": { "name": branch_name },
			"repository": {
				"full_name": "%s" % source_repo
			}
		},
		"destination": { "branch": { "name": target_branch } },
		"description": pull_body
	}

	if reviewer:
		if is_valid_uuid(reviewer):
			params['reviewers'] = [{'uuid': reviewer}]
		else:
			params['reviewers'] = [{'username': reviewer}]

	print color_text("Sending pull request to %s" % reviewer, 'status')
	if DEBUG:
		print json.dumps(params, sort_keys=True, indent=4)

	pull_request = bitbucket_json_request(url, params)

	if pull_request.get('error'):
		raise UserWarning("%s " % json.dumps(pull_request.get('error'), sort_keys=True, indent=4))

	display_pull_request(pull_request)
	print

	print color_text("Pull request submitted", 'success')
	print
	display_status()

	if submitOpenBitbucket:
		url = pull_request.get('links').get('html').get('href')
		open_URL(url)


def main():
	# parse command line options
	try:
		opts, args = getopt.gnu_getopt(sys.argv[1:], 'hqdr:u:t:', ['help', 'quiet', 'debug', 'repo=', 'reviewer=', 'targetbranch='])
	except getopt.GetoptError, e:
		raise UserWarning("%s\nFor help use --help" % e)

	if len(args) > 0 and args[0] == 'help':
		command_help()
		sys.exit(0)

	# load git options
	load_options()

	global auth_string
	global DEBUG

	repo_name = None
	reviewer_repo_name = None
	target_branch = None
	DEBUG = options['debug']

	username = os.popen('git config bitbucket.user').read().strip()
	auth_token = os.popen('git config bitbucket.token').read().strip()

	if len(username) == 0:
		username = raw_input("bitbucket username: ").strip()
		os.system("git config --global bitbucket.user %s" % username)

	if len(auth_token) == 0:
		print "Please go to https://bitbucket.org/account/admin to find your App password"
		auth_token = raw_input("bitbucket App token: ").strip()
		os.system("git config --global bitbucket.token %s" % auth_token)

	auth_string = base64.encodestring('%s:%s' % (username, auth_token)).replace('\n', '')

	info_user = username
	submitOpenBitbucket = options['submit-open-bitbucket']

	# process options
	for o, a in opts:
		if o in ('-h', '--help'):
			command_help()
			sys.exit(0)
		elif o in ('-q', '--quiet'):
			submitOpenBitbucket = False
		elif o in ('-r', '--repo'):
			if re.search('/', a):
				repo_name = a
			else:
				repo_name = get_repo_name_for_remote(a)
		elif o in ('-u', '--reviewer'):
			reviewer_repo_name = a
		elif o in ('-t', '--targetbranch'):
			target_branch = a
		elif o in ('-d', '--debug'):
			DEBUG = True

	# get default repo name
	if repo_name is None or repo_name == '':
		repo_name = get_default_repo_name()

	if reviewer_repo_name is None or reviewer_repo_name == '':
		reviewer_repo_name = os.popen('git config bitbucket.reviewer').read().strip()

	# process arguments
	if len(args) > 0:
		if args[0] == 'help':
			command_help()
			sys.exit(0)
		elif args[0] == 'open':
			if len(args) >= 2:
				command_open(repo_name, args[1])
			else:
				command_open(repo_name)
		elif args[0] == 'submit':
			pull_body = None
			pull_title = None

			if len(args) >= 2:
				pull_body = args[1]

			if len(args) >= 3:
				pull_title = args[2]

			command_submit(repo_name, username, target_branch, reviewer_repo_name, pull_body, pull_title, submitOpenBitbucket)
		else:
			command_show(repo_name)
	else:
		command_show(repo_name)


if __name__ == "__main__":
	try:
		main()
	except UserWarning, e:
		print color_text(e, 'error')
		sys.exit(1)
