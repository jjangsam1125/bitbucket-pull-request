# Bitbucket Pull Request Tool

Bitbucket command tool to help many common tasks involving pull requests.

![picture](screenshot/pr_tool_screenshot.png)

## Requirement
* Python 2.7+

## Install
* First clone this repository to a directory of your choice.


	`$ git clone git@bitbucket.org:jjangsam1125/bitbucket-pull-request.git`

* Then edit your bash profile and add the following line:


	`alias pr="${YOUR_DIRECTORY}/bitbucket-pull-request/bitbucket-pull-request.py"`

## Obtain Bitbucket App password

* Go to https://bitbucket.org/account/user/${user_id}/app-passwords
* Click "Create app password"
* Check "write" checkbox for Repositories and Pull requests
* Click "Create"
* A popup will show new App password. You will not be able to view this password again once you close this window, so be sure to record it.

## Automatic Setup
* Execute pr tool


	`$ pr`

* Enter bitbucket username


	`bitbucket username: jjangsam1125`

* Enter bitbucket App token obtained from previous step


	`bitbucket App token: XXXXXXXXXXXXXXXXXXXX`

## Manual Setup
* Open git config manually


	`$ vim ~/.gitconfig`

* Add following lines


	`[bitbucket]`


	`	user = your bitbucket username`


	`	token = your bitbucket App password`

# Usage

* To see a list of all possible commands, run:


	`$ pr help`


* Usage:


	`$ pr [<options>] <command> [<args>]`

* Options:
	* `-h, --help`				
	> Display help message.


	* `-r <repo>, --repo <repo>`
	> Use this repo instead of upstream or origin. If no repo option is provided, it will use 'upstream' as repo name. In case there is no remote upstream found in the current repo, it will use 'origin' as repo name.


	* `-d --debug`
	> Enable debug mode.


	* `-u <reviewer>, --reviewer <reviewer>`
	> Send pull request and assign it to this reviewer. This can either be the username or the UUID of the account


	* `-t <targetbranch>, --targetbranch <targetbranch>`
	> Send pull requests to this targetbranch.


* Commands:
	* `no command`
	> Displays a list of the open pull requests on this repository.

	* `help`
	> Displays this message.

	* `open`
	> Opens bitbucket pull request page for the current repo.

	* `open [<pull request ID>]`
	> Opens either the current pull request or the specified request on bitbucket.

	* `submit [<pull body>] [<pull title>]`
	> Pushes a branch and sends a pull request to the user's reviewer on bitbucket.
